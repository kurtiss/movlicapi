#!/usr/bin/env python

import bs4
import datetime
import itertools
import json
import pprint
import requests
import xlwt


ratings = ["R", "PG-13", "PG", "G", "TV-G", "TV-Y", "TV-Y7", "TV-PG", "TV-14", "TV-MA", "NR"]
start_year = 1915
today = datetime.datetime.today()
end_year = today.year


def movlic_titles():
    for year in range(1915, end_year + 1):
        print year
        for result in movlic_results(year):
            yield result

def movlic_results(release_year):
    result_set = query_movlic(release_year, "")

    if results_could_be_missing(result_set):
        result_sets = []
        for rating in ratings:
            rated_result_set = query_movlic(release_year, rating)

            if results_could_be_missing(rated_result_set):
                raise NotImplementedError("Cannot sufficiently drill down into titles from year {0} with rating '{1}'.".format(release_year, rating))

            result_sets.append(rated_result_set)
    else:
        result_sets = [result_set]

    for nr in normalized_results(result_sets):
        yield nr

def results_could_be_missing(results):
    return results["ResultsCount"] >= results["MaxResultsCount"]

def query_movlic(release_year, rating):
    result = requests.get(
        "http://library.movlic.com/search-results",
        params = dict(
            title = "",
            actor = "",
            director = "",
            releaseYear = release_year,
            rating = rating,
            format = "",
            Format = ["DVD", "DIG", "VHS", "BVD", "35MM", "16MM"],
            genre = "",
            includeFormats = "true"
        )
    )

    soup = bs4.BeautifulSoup(result.text, "html.parser")
    wc_article = soup.find("article", class_ = "widget-swank-search-widget")
    wc_sript = wc_article.findChild("script")
    sch_results_text = wc_article.findChild("script").text
    sch_results_text = "".join(sch_results_text.partition("{")[1:])
    sch_results_text = "".join(sch_results_text.rpartition("}")[:2])
    results = json.loads(sch_results_text)

    return results

def normalized_results(result_sets):
    by_id = {}

    for result_set in result_sets:
        for result in result_set["Results"]:
            result_id = result["ID"]

            if result_id not in by_id:
                normalized = by_id[result_id] = dict(
                    ID = result_id,
                    MPAA_rating = result["MpaaRating"],
                    year = result["Year"],
                    synopsis = result["Synopsis"],
                    running_time = int(result["RunTime"]),
                    title = result["Title"],
                    thumbnail_urls = "\n".join(filter(None, result["ThumbnailImageUrls"])),
                    trailer_urls = "\n".join(filter(None, result["TrailerUrls"])),
                    publishers = "\n".join(filter(None, result["Publisher"])),
                    available_formats = u", ".join(x.strip() for x in result["AvailableFormats"] if x.strip())
                )
                yield normalized

if __name__ == "__main__":
    workbook = xlwt.Workbook(encoding = "utf-8")
    sheet = workbook.add_sheet("Swank Titles")
    filename = "SwankTitles_{0}.xslx".format(today.isoformat().split("T")[0].replace("-", "_"))

    try:
        for row, title in enumerate(movlic_titles()):
            cell = itertools.count()
            sheet.write(row, cell.next(), title["title"])
            sheet.write(row, cell.next(), title["year"])
            sheet.write(row, cell.next(), title["running_time"])
            sheet.write(row, cell.next(), title["MPAA_rating"])
            sheet.write(row, cell.next(), title["synopsis"])
            sheet.write(row, cell.next(), title["publishers"])
            sheet.write(row, cell.next(), title["trailer_urls"])
            sheet.write(row, cell.next(), title["thumbnail_urls"])
            sheet.write(row, cell.next(), title["available_formats"])
            sheet.write(row, cell.next(), title["ID"])
    finally:
        workbook.save(filename)
